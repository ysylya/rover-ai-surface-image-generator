# Martian rover AI simulation
#### _Part 1/2: Surface image generator_

This project simulates a martian rover detecting obstacles (rocks) on the surface.
The simulation has two parts:

* 1/2: Surface image generator
* 2/2: [Obstacle detector](https://gitlab.com/ysylya/rover-ai-obstacle-detector)

This is Part 1, where I implemented a surface image generator addon for Blender. This addon can generate tons of similar, yet different images relatively fast, based on the user's input.

First, the addon generates a scene, which has a surface and rocks on it. (The rocks are currently simple spheres.) Then the scene is being rendered and the rendered image(s) saved.
The addon is very configurable, for example one can set:

* Number of rendered scenes
* Number and size of bumps on the surface 
    * There could be more layers of bumps, e.g. some big bumps, more smaller ones
    * Locations of the bumps are random at every scene
    * Based on Perlin noise
* Number interval and size interval of the rocks 
    * Numbers and sizes are random in the given interval
    * There could be more size and number groups of rocks, e.g. a few big, lots of smaller
* A generated scene can be rendered more than once, with different, random Sun positions

For teaching the Obstacle detector, for every scene, at least two images are generated: a "real" camera picture and a BW coded image, where white shows the rocks on the "real" image.

These are some example rendered scenes:

![011](img/01/Surface1.png) ![012](img/01/MaskedRocks1.png)

![021](img/02/Surface1.png) ![022](img/02/MaskedRocks1.png)

![031](img/03/Surface1.png) ![032](img/03/MaskedRocks1.png)

![041](img/04/Surface1.png) ![042](img/04/MaskedRocks1.png)

![051](img/05/Surface1.png) ![052](img/05/MaskedRocks1.png)
