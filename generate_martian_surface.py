import bpy
import random
from timeit import default_timer as timer
import math
import bmesh
import gc
import time


def debugprint(name, value):
    print(name + ": " + str(value))


def generate_terrain_simple(obj, size_meters):
    num_hump_big = random.randint(1, size_meters/2)
    
    for i in range(num_hump_big):
        vert_num = random.randint(0, len(obj.data.vertices))
        
        bpy.ops.object.mode_set(mode='OBJECT')
        obj.data.vertices[vert_num].select = True
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.transform.translate(value=(0, 0, 2.0), orient_type='GLOBAL', 
                                    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', 
                                    constraint_axis=(False, False, True), mirror=True, 
                                    use_proportional_edit=True, proportional_edit_falloff='SMOOTH', 
                                    proportional_size=5.0, use_proportional_connected=False, 
                                    use_proportional_projected=False)
        bpy.ops.object.mode_set(mode='OBJECT')
        obj.data.vertices[vert_num].select = False
        

class Terrain:
    class PrecomputedGradients:
        def __init__(self, grid_divs_x, grid_divs_y, height_max):
            self.grid_divs_x = grid_divs_x
            self.grid_divs_y = grid_divs_y
            self.height_max = height_max
            self.array = []
            self.generate_precomputed_gradients()
        
        
        @classmethod
        def init_from_sizes(cls, 
                            one_grid_div_size_in_meters_x, terrain_size_in_meters_x, 
                            one_grid_div_size_in_meters_y, terrain_size_in_meters_y,
                            height_max):
            grid_divs_x = int(terrain_size_in_meters_x / one_grid_div_size_in_meters_x)
            grid_divs_y = int(terrain_size_in_meters_y / one_grid_div_size_in_meters_y)
            
            return cls(grid_divs_x, grid_divs_y, height_max)
        
    
        def normalize(self, v):
            magn = (v[0] ** 2 + v[1] ** 2) ** 0.5
            
            if magn == 0.0:
                magn = 0.000001
                
            return [v[0] / magn, v[1] / magn]
    
    
        def generate_random_2d_gradient(self):
            x = random.uniform(-1.0, 1.0)
            y = random.uniform(-1.0, 1.0)
            return self.normalize([x, y])
        
        
        def generate_precomputed_gradients(self):
            for x in range(self.grid_divs_x+1): # +1, otherwise we couldn't compute from two values at the edge 
                self.array.append([])
                for y in range(self.grid_divs_y+1):
                    self.array[x].append(self.generate_random_2d_gradient())
        
    
        def print_gradient_array(self):
#            with open(f"/tmp/mars/precom_grad_debug_{id(self)}.txt", "w") as fp:
            for x in range(self.grid_divs_x+1):
                for y in range(self.grid_divs_y+1):
                    data = f"x:{x}, y:{y}, {self.array[x][y]}"
                    print(data)
#                        fp.write(data + "\n")

        
    def __init__(self, size_meters_x, size_meters_y):
        self.size_meters_x = size_meters_x
        self.size_meters_y = size_meters_y

        self.precalculated_gradients = []
        
        # for the heightmap generation, vmin, vmax are vertex heights
        self.obj_ground = None
        self.vmin = 0.0
        self.vmax = 0.0
        
    
    # call this before generating the ground
    def setup_ground_add_random_bumps(self, bump_size_meters_x, bump_size_meters_y, height):
        self.precalculated_gradients.append(
            self.PrecomputedGradients.init_from_sizes(
                bump_size_meters_x, self.size_meters_x, 
                bump_size_meters_y, self.size_meters_y, 
                height
            )
        )
    
    
    # call setup_ground_add_random_bumps before this for a much better looking ground
    def generate_ground(self, collection):
        segments_x = (self.size_meters_x + 1) * 5
        segments_y = (self.size_meters_y + 1) * 5

        # TODO for non-NxN grid, we have to scale according to size_meters_x and _y, then apply
        
        # we need a random(ish) name bc this way we can use randomness in the shader
        name = f'Ground_RND{int(time.time()*10**7)}'
        mesh = bpy.data.meshes.new(name)
        self.obj_ground = bpy.data.objects.new(name, mesh)
        
        bm = bmesh.new()
        bmesh_grid_size = self.size_meters_x / 2 # bmesh is treating size differently than bpy.ops
        bmesh.ops.create_grid(bm, x_segments=segments_x, y_segments=segments_y, size=bmesh_grid_size)
        
#        vmin = 0.0
#        vmax = 0.0
        for vert in bm.verts:
            vert.co[2] = self.get_height_at(vert.co[0], vert.co[1])
            if vert.co[2] < self.vmin:
                self.vmin = vert.co[2]
            if vert.co[2] > self.vmax:
                self.vmax = vert.co[2]
        
        uv_layer = bm.loops.layers.uv.verify()
        for face in bm.faces:
            face.smooth = True
            for loop in face.loops:
                loop_uv = loop[uv_layer]
                loop_uv.uv = loop.vert.co.xy
            
        bm.to_mesh(mesh)
        bm.free()
        
        mod_subsurf_adaptive = self.obj_ground.modifiers.new('Subsurf Adaptive', 'SUBSURF')
        mod_subsurf_adaptive.levels = 0
        mod_subsurf_adaptive.render_levels = 0
        self.obj_ground.cycles.use_adaptive_subdivision = True
        
        self.obj_ground.data.materials.append(bpy.data.materials['MatGround'])
        
        collection.objects.link(self.obj_ground)
        
        # to view shading changes
        self.obj_ground.data.update() 
#        mesh.update()
    
    
    def generate_single_rock(self, collection, rock_size_min, rock_size_max):
        rock_size = random.uniform(rock_size_min, rock_size_max)
        rock_location_x = random.uniform(-self.size_meters_x / 2.0, self.size_meters_x / 2.0)
        rock_location_y = random.uniform(-self.size_meters_y / 2.0, self.size_meters_y / 2.0)
        portion_in_soil = 0.33334
        rock_location_z = self.get_height_at(rock_location_x, rock_location_y) + rock_size * portion_in_soil

        mesh = bpy.data.meshes.new('Rock')
        obj = bpy.data.objects.new("Rock", mesh)
        obj.location = (rock_location_x, rock_location_y, rock_location_z)
        
        bm = bmesh.new()
        bmesh.ops.create_cube(bm, size=rock_size)

        for face in bm.faces:
            face.smooth = True
            
        bm.to_mesh(mesh)
        
        bm.free()
        
        obj.data.materials.append(bpy.data.materials['MatRock'])
        
        # to view shading changes
    #    obj.data.update() 
        
        mod_subsurf = obj.modifiers.new('Subsurf', 'SUBSURF')
        mod_subsurf.levels = 0
        mod_subsurf.render_levels = 4
        
        
        obj_mod_displace_helper = bpy.data.objects.new('Rock Helper', None)
        obj_mod_displace_helper.location = (
            random.uniform(-100.0, 100.0), 
            random.uniform(-100.0, 100.0), 
            random.uniform(-100.0, 100.0)
        )
        obj_mod_displace_helper.parent = obj
        
#        mod_displace = obj.modifiers.new('Displace', 'DISPLACE')
#        mod_displace.texture = bpy.data.textures['test']
#        mod_displace.texture_coords = 'OBJECT'
#        mod_displace.texture_coords_object = obj_mod_displace_helper
        
        mod_subsurf_adaptive = obj.modifiers.new('Subsurf Adaptive', 'SUBSURF')
        mod_subsurf_adaptive.levels = 0
        mod_subsurf_adaptive.render_levels = 0
        obj.cycles.use_adaptive_subdivision = True
        
        collection.objects.link(obj)
        
        collection.objects.link(obj_mod_displace_helper)
        obj_mod_displace_helper.hide_set(True)
    
    
    def generate_rocks(self, collection, amount, rock_size_min, rock_size_max):
        for i in range(amount):
            self.generate_single_rock(collection, rock_size_min, rock_size_max)
    
    
    def map_interval(self, from_low, from_high, to_low, to_high, value):
        # x' = ((x - a) / (b - a)) * (d - c) + c
        if (from_high - from_low) < 0.000001:
            return 0.0 # TODO
        else:
            return ((value - from_low) / (from_high - from_low)) * (to_high - to_low) + to_low

    
    # to the size of the gradient array
    def __map_coords_to_array_size(self, precomputed_gradients, x, y):
        # size_meters_?/2.0 bc if the size is eg 20, it goes from -10 to 10
        # grid_divs_? - 1 bc 0-based indexing
        x = self.map_interval(-self.size_meters_x / 2.0, self.size_meters_x / 2.0, 
                              0, precomputed_gradients.grid_divs_x - 1, 
                              x)
        y = self.map_interval(-self.size_meters_y / 2.0, self.size_meters_y / 2.0, 
                              0, precomputed_gradients.grid_divs_y - 1, 
                              y)
        return x, y
        
    
    def dot(self, v1, v2):
        dot_prod = v1[0]*v2[0] + v1[1]*v2[1]
        normalized = dot_prod / (2.0**0.5) # 
        return dot_prod
    
    
    def linear_interpolation(self, x, y, weight):
        return (1.0 - weight) * x + weight * y 
    
    
    def poly_interpolation(self, x, y, weight):
        return weight**2 * (3.0 - 2.0*weight) * (y - x) + x;
    
    
    def interpolate(self, x, y, weight):
        return self.poly_interpolation(x, y, weight)
    
    
    def __calc_height_at_level(self, precomputed_gradients, x, y):
        # the grid is created at the center, we need to map the coords to the array indices
        x, y = self.__map_coords_to_array_size(precomputed_gradients, x, y)

        # getting the corresponding grid coords
        grid_x1 = int(x)
        grid_x2 = grid_x1 + 1
        grid_y1 = int(y)
        grid_y2 = grid_y1 + 1
        
        offset_from_grid_x1 = grid_x1 - x
        offset_from_grid_x2 = grid_x2 - x
        offset_from_grid_y1 = grid_y1 - y
        offset_from_grid_y2 = grid_y2 - y
        
        dx1y1 = [offset_from_grid_x1, offset_from_grid_y1]
        dx1y2 = [offset_from_grid_x1, offset_from_grid_y2]
        dx2y1 = [offset_from_grid_x2, offset_from_grid_y1]
        dx2y2 = [offset_from_grid_x2, offset_from_grid_y2]
        
        weight_x = x - float(grid_x1)
        weight_y = y - float(grid_y1)
        
        n1 = self.dot(precomputed_gradients.array[grid_x1][grid_y1], dx1y1)
        n2 = self.dot(precomputed_gradients.array[grid_x1][grid_y2], dx1y2)
        interp_y1 = self.interpolate(n1, n2, weight_y)

        n1 = self.dot(precomputed_gradients.array[grid_x2][grid_y1], dx2y1)
        n2 = self.dot(precomputed_gradients.array[grid_x2][grid_y2], dx2y2)
        interp_y2 = self.interpolate(n1, n2, weight_y)
        
        interp_x = self.interpolate(interp_y1, interp_y2, weight_x)
        return interp_x * precomputed_gradients.height_max
    
    
    def get_height_at(self, x, y):
        height = 0.0
        for prec_grad in self.precalculated_gradients:
            height += self.__calc_height_at_level(prec_grad, x, y)
        return height
        

    def height_to_svg(self, resolution):
        grid = self.obj_ground
        filename = "/home/suli/grid.svg"
        svg = f'<svg width="{resolution}" height="{resolution}">\n'
        for i in range(len(grid.data.vertices)):
            vert = grid.data.vertices[i]
            # size_meters_?/2.0 bc if the size is eg 20, it goes from -10 to 10
            w = int(self.map_interval(-self.size_meters_x/2, self.size_meters_x/2, 0, resolution, vert.co[0]))
            h = int(self.map_interval(-self.size_meters_y/2, self.size_meters_y/2, 0, resolution, vert.co[1]))
            color = int(self.map_interval(self.vmin, self.vmax, 0, 255, vert.co[2]))
            color = hex(color)
            color = color[2:]
            svg += f'<rect x="{w}" y="{h}" width="10" height="10" style="fill:#{color}{color}{color};stroke-width:0;fill-opacity:1.0" />'
        svg += '</svg>'
        with open(filename, "w") as fp:
            fp.write(svg)
    

class SunRandomizer:
    def __init__(self, obj, angle_min, angle_max):
        self.obj = obj
        self.angle_min = angle_min
        self.angle_max = angle_max
    
    def randomize(self):
        angle_x = random.uniform(self.angle_min, self.angle_max)
        angle_y = random.uniform(self.angle_min, self.angle_max)
        self.obj.rotation_euler[0] = angle_x
        self.obj.rotation_euler[1] = angle_y
        

class Renderer:
    def __init__(self, filepath_wo_numbering, len_numbering, mask_collection):
        bpy.context.scene.view_layers["View Layer"].use_pass_object_index = True
        self.filepath_wo_numbering = filepath_wo_numbering
        self.len_numbering = len_numbering
        self.mask_collection = mask_collection

        
    def render(self, i):
        for obj in self.mask_collection.objects:
            obj.pass_index = 1
        filepath = self.filepath_wo_numbering + f'_{int(i+1):0{self.len_numbering}d}'
        
        bpy.data.scenes["Scene"].node_tree.nodes["File Output"].base_path = filepath
        bpy.ops.render.render(write_still = False)


# create one if necessary
def get_collection(collection_name):
    collection = bpy.data.collections.get(collection_name)
    if collection is None:
        collection = bpy.data.collections.new(collection_name)
        bpy.context.scene.collection.children.link(collection)
    return collection


def clean_collection(collection):
    try:
        bpy.ops.object.mode_set(mode='OBJECT') 
    except:
        # what if there were no objects
        pass
    
    bpy.ops.object.select_all(action='DESELECT')
    
    for obj in collection.all_objects:
        obj.hide_set(False)
        obj.select_set(True)
    
    bpy.ops.object.delete(use_global=False)
    
#    for obj in collection.all_objects:
#        bpy.data.objects.remove(obj, do_unlink=True)
    

def generate_terrain(collection_ground, collection_rocks):
    size_meters = 30.0
    
    terrain = Terrain(size_meters, size_meters)

    terrain.setup_ground_add_random_bumps(20.0, 20.0, 3.0)
    terrain.setup_ground_add_random_bumps(10.0, 10.0, 4.0)
    terrain.setup_ground_add_random_bumps(5.0, 5.0, 1.0)
    terrain.setup_ground_add_random_bumps(3.0, 3.0, 0.5)
    terrain.setup_ground_add_random_bumps(0.5, 0.5, 0.05)
    terrain.setup_ground_add_random_bumps(0.15, 0.15, 0.025)
    terrain.generate_ground(collection_ground)
    
    terrain.generate_rocks(collection_rocks, 20, 0.45, 0.75)
    terrain.generate_rocks(collection_rocks, 10, 0.1, 0.25)
    terrain.generate_rocks(collection_rocks, 500, 0.1, 0.2)

#    terrain.generate_rocks(collection_rocks, 1, 1.0, 1.0)
#    terrain.height_to_svg(1024)
    bpy.ops.object.select_all(action='DESELECT')

   
def main():
    start = timer()
#    random.seed(10)
    collection_ground = get_collection('Ground')
    collection_rocks = get_collection('Rocks')
    
    obj_sun = bpy.data.objects['Sun']
    sun_randomizer = SunRandomizer(obj_sun, -math.pi/2, math.pi/2)
    
    number_of_terrain_generation = 1
    number_of_sun_iterations = 1 # at least 1 iteration has to be made
        
    number_of_renders = number_of_terrain_generation * number_of_sun_iterations
    output_name_numbering_length = int(math.log(number_of_renders, 10)) + 1
    renderer = Renderer('/tmp/martian_surface/martian_surface', output_name_numbering_length, collection_rocks)
    
    time_cleaning = 0
    time_terrain_generation = 0
    time_render = 0
    for i in range(number_of_terrain_generation):
        
        time_cleaning_start = timer()
        clean_collection(collection_ground)
        clean_collection(collection_rocks)
        time_cleaning += timer() - time_cleaning_start
        
        time_terrain_generation_start = timer()
        generate_terrain(collection_ground, collection_rocks)
        time_terrain_generation += timer() - time_terrain_generation_start
        
        for j in range(number_of_sun_iterations): 
#            sun_randomizer.randomize()
            step = i * number_of_sun_iterations + j
            
            time_render_start = timer()
#            renderer.render(step)
            time_render += timer() - time_render_start
            
            gc.collect()
            
            print(f'{(step+1)/number_of_renders:.0%}')
            
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        
    time_cleaning /= max(1, number_of_terrain_generation)
    time_terrain_generation /= max(1, number_of_terrain_generation)
    time_render /= max(1, number_of_renders)
    
    end = timer()
    print(f'Avg. cleaning time: {time_cleaning:.2f} seconds')
    print(f'Avg. terrain generation time: {time_terrain_generation:.2f} seconds')
    print(f'Avg. render time: {time_render:.2f} seconds')
    print(f'Avg. time/picture: {(end-start)/number_of_renders:.2f} seconds')
    print(f'Avg. time/picture (1 sun iteration): {time_cleaning+time_terrain_generation+time_render:.2f} seconds')
    print(f'Total time: {end-start:.2f} seconds')
    print("---")


main()
